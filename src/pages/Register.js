import {useState, useEffect} from 'react'
import {Form, Button, Row, Col, Container} from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'

export default function Register(){
	const [fN, setFN] = useState("")
	const [lN, setLN] = useState("")
	const [email, setEmail] = useState("")
	const [pw, setPW] = useState("")
	const [vpw, setVPW] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)

	const navigate = useNavigate();

	
	useEffect(() => {

		
		if((fN !== "" && lN !== "" && email !== "" && pw !== "" && vpw !== "") && (pw == vpw)){

			setIsDisabled(false)

		} else {
			//if all input fields are empty, keep the state of the button to true
			setIsDisabled(true)
		}

		
	}, [fN, lN, email, pw, vpw])

	const registerUser = (e) => {
		e.preventDefault()

		fetch('https://whispering-scrubland-10370.herokuapp.com/users/email-exists', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log("responseEmailExist", response)	//false
			if(!response){
				//send request to register
				fetch('https://whispering-scrubland-10370.herokuapp.com/users/register', {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: fN,
						lastName: lN,
						email: email,
						password: pw
					})
				})
				.then(response => response.json())
				.then(response => {
					console.log("responseRegisterUser", response)

					if(response){
						alert('Registration Successful.')

						//redirect
						navigate('/login')
					} else
					{
						alert('Something went wrong. Please try again')
					}
				})


			} else{
				alert(`Oh no! This user already exists`)
			}
		})
	}

	return(
		<Container className="mt-5" >

		 	<h2 className="text-center font-weight-bold mb-5">Register an account</h2>

			<Row className="rowBackground no-gutters">
				<Col lg={5}>
					 <img src='./../images/tomato.jpg' className="img-fluid loginImage"/>
				</Col>

				<Col lg={7} className="px-5 pt-5">
					<Form onSubmit={(e) => registerUser(e) }>
						<div>
						<Col lg={7}>
						<Form.Group className="mb-3">
							<Form.Label>First Name</Form.Label>
					    	<Form.Control 
								className="my-3 p-4"
					    		type="text" 
					    		value={fN}
					    		onChange={(e) => setFN(e.target.value)}
					    	/>
						</Form.Group>
						</Col>	
						</div>
						

						<div>
						<Col lg={7}>
						<Form.Group className="mb-3">
							<Form.Label>Last Name</Form.Label>
					    	<Form.Control 
								className="my-3 p-4"
					    		type="text" 
					    		value={lN}
					    		onChange={(e) => setLN(e.target.value)}
					    	/>
						</Form.Group>
						</Col>	
						</div>
						

						<div>
						<Col lg={7}>
						<Form.Group className="mb-3">
							<Form.Label>Email address</Form.Label>
					    	<Form.Control 
								className="my-3 p-4"
					    		type="email" 
					    		value={email}
					    		onChange={(e) => setEmail(e.target.value)}
					    	/>
						</Form.Group>
						</Col>	
						</div>
						


						<div>
						<Col lg={7}>
						<Form.Group className="mb-3">
					    	<Form.Label>Password</Form.Label>
					    	<Form.Control 
								className="my-3 p-4"
					    		type="password" 
					    		value={pw}
					    		onChange={(e) => setPW(e.target.value)}
					    	/>
						</Form.Group>
						</Col>	
						</div>
						


						<div>
						<Col lg={7}>
						<Form.Group className="mb-3">
					    	<Form.Label>Verify Password</Form.Label>
					    	<Form.Control 
								className="my-3 p-4"
					    		type="password" 
					    		value={vpw}
					    		onChange={(e) => setVPW(e.target.value)}
					    	/>
						</Form.Group>
						</Col>	
						</div>
						


						<div>
						<Col lg={7}>
						<Button 
							className="button"
							variant="dark" 
							type="submit"
							disabled={isDisabled}
						>
							Register
						</Button>
						</Col>	
						</div>
						
					</Form>
				</Col>
			</Row>
		</Container>
	)
}
