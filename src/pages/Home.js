import {Fragment} from 'react'
import Banner from './../components/Banner'
import Highlights from './../components/Highlights'


export default function Home(){

	const data = {
		title: "Green Deli",
		description: "Making you healthier since day 1",
		destination: "/products",
		buttonDesc: "Shop Now"
	}

	return(
		// render navbar, banner & footer in the webpage via home.js
		<Fragment>
			<Banner bannerProp={data}/>
			<Highlights/>
		</Fragment>
	)
}
