import { Fragment, useContext, useEffect, useState } from "react";
import { Row, Col, Container } from "react-bootstrap";
import ProductCard from './../components/ProductCard'
import AdminPage from './AdminPage';
import UserContext from './../UserContext'
const admin = localStorage.getItem('admin')
const token = localStorage.getItem('token')

export default function Products () {
    const { state, dispatch } = useContext(UserContext)
	console.log(state)

	const [products, setProducts] = useState([])

	useEffect( () => {
		if(admin === "false"){
			fetch(`https://whispering-scrubland-10370.herokuapp.com/products/isOffered`, {
				method: "GET",
				headers:{
					"Authorization": `Bearer ${token}`
				}
			})
			.then(response => response.json())
			.then(response => {

				if(token !== null){
					dispatch({type: "USER", payload: true})
				}
				
				setProducts(
					response.map(product => {
						console.log("mapProduct", product)
						return <ProductCard key={product._id} productProp={product}/>
					})
				)
			})
		}

	}, [])

    return(
		
		<Fragment>
			
			{
				admin === "false" ?
				
					
						<Fragment >
						<Container>

						<div className="featuredProducts">
						<h1 className="text-center"> Featured Products </h1>
						</div>
						
						<Row   md={3} className="g-4">
						
							{products}
							
						</Row>
						</Container>
						
						</Fragment>
					
				
				:
					<Fragment>
						<AdminPage />
					</Fragment>
					
			}
		</Fragment>
	)
}
