// product page links to product/${productID}

// response fetch /orders/create

import { useContext, useEffect, useState } from 'react'
import UserContext from './../UserContext'
import { Container, Row, Col, Card, Button } from 'react-bootstrap'
import { useParams, useNavigate, Link } from 'react-router-dom'

import { useCart } from './../components/CartPage'
import { useDispatchCart } from './../components/CartPage'

const token = localStorage.getItem('token')
// console.log(typeof token)

// const Product = ({ product }) => {
// 	const addToCart = (item) => {
// 		
// 	}
// }




export const ProductPage = ({product}) =>{
	const dispatchOne = useDispatchCart()
	//newly added
	const addToCart = (item) => {
		dispatchOne({ type: "ADD", item });
		// console.log("itemCart", item)
	  };
	const items = useCart()
	
	const { dispatch } = useContext(UserContext)

	const { productId } = useParams()
	console.log("productId",productId)

	const navigate = useNavigate()


	const [productName, setProductName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)


	const fetchProducts = () => {
        //fetch to product id (retrieve an order route)
		fetch(`https://whispering-scrubland-10370.herokuapp.com/products/${productId}`, {
			method: "GET",
			headers:{
				"Authorization": `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log("responseFetchProdId", response)

			setProductName(response.productName)
			setDescription(response.description)
			setPrice(response.price)
		})
	}

	useEffect(() => {
		if(token !== null){
			dispatch({type: "USER", payload: true})
		}

		fetchProducts()

	}, [])

	

	return (
		

		<Container className="productContainer">
			{/* new */}
			<Link to="/cart"> <h1 className="mb-5 text-decoration-none"> Shopping Cart ({items.length}) </h1> </Link> 
			<Row className="no-gutters">

				
				<Col xs={12} md={6}>
					<img src='./../images/vegetable.jpg' className="img-fluid addToCartImg"/>
				</Col>

				<Col xs={12} md={6}>
					<Card className="m-5" border="success" >
					  <Card.Body>
					    <Card.Title>{productName}</Card.Title>
					    <Card.Subtitle>Description:</Card.Subtitle>
					    <Card.Text>
					      {description}
					    </Card.Text>
					    <Card.Subtitle>Price(Php):</Card.Subtitle>
					    <Card.Text>
					    	{price}
					    </Card.Text>

					    <Button 
							onClick={() => addToCart(product)}
							
					    	className="btn btn-success addToCartButton"
					    	
					    >Add to Cart</Button>
						
						<Button 
					    	className="btn btn-success"
					    >
						<Link className="returnToShopButton" to={`/products`}> Return to shop </Link> 
						</Button>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)

}
export default ProductPage;