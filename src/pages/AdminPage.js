import { useEffect, useState, Fragment, useContext } from 'react'
import { Container, Table, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from './../UserContext'

export default function AdminPage() {

	const [allProducts, setAllProducts] = useState([])

	const { dispatch } = useContext(UserContext)

	const fetchData = () => {
		fetch(`https://whispering-scrubland-10370.herokuapp.com/products`, {
			method: "GET",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			dispatch({type: "USER", payload: true})

			setAllProducts( response.map(product => {
				console.log("setAllProducts", product)

				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.productName}</td>
						<td>{product.price}</td>
						<td>{product.isOffered ? "Active" : "Inactive"}</td>
						<td>
							{
								product.isOffered ?
									<Button 
										className="btn btn-danger mx-2"
										onClick={ () => handleArchive(product._id) }
									>
										Archive
									</Button>
								:
									<Fragment>
										<Button 
											className="btn btn-success mx-2"
											onClick={ () => handleUnarchive(product._id)}
										>
												Unarchive
										</Button>
										<Button 
											className="btn btn-secondary mx-2"
											onClick={ () => handleDelete(product._id) }
										>
											Delete
										</Button>
									</Fragment>
							}
						</td>
					</tr>
				)
			}))
		})
	}

	useEffect(() => {
		fetchData()

	}, [])

	const handleArchive = (productId) =>{
		console.log(productId)
		fetch(`https://whispering-scrubland-10370.herokuapp.com/products/${productId}/archive`, {
			method: "PATCH",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()

				alert('The product was successfully archived!')
			}
		})
	}

	const handleUnarchive = (productId) =>{
		console.log(productId)
		fetch(`https://whispering-scrubland-10370.herokuapp.com/products/${productId}/unarchive`, {
			method: "PATCH",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			//console.log(response)

			if(response){
				fetchData()
				
				alert('The product was successfully Unarchived!')
			}
		})
	}

	const handleDelete = (productId) =>{
		console.log(productId)
		fetch(`https://whispering-scrubland-10370.herokuapp.com/products/${productId}/delete-product`, {
			method: "DELETE",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()
				
				alert('The product was successfully Deleted!')
			}
		})
	}

	return(
		<Container className="container">
			<h1 className="my-5 p-2 text-center featuredProducts">Products Dashboard</h1>
			<div className="text-right">
				<Link className="btn btn-success m-2" to={`/addProduct`}>Add a New Product</Link>
				
			</div>
			<Table className="table table-borderless table-dark">
				<thead>
					<tr>
						<th>Product ID</th>
						<th>Name</th>
						<th>Price(Php)</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ allProducts }
				</tbody>
			</Table>
		</Container>
	)
}
