import { Card } from "react-bootstrap";
import React from "react";
import { useCart, useDispatchCart } from "../components/CartPage";
import ProductCard from "./../components/ProductCard";



export default function Cart() {
  const items = useCart();
  const dispatchOne = useDispatchCart();
  const totalPrice = 0;

  const handleRemove = (index) => {
    dispatchOne({ type: "REMOVE", index });
  };

 
  return (
    <main>
        <h3 className="m-5"> Oops! It seems that the cart page is still under construction </h3>
    </main>
  );
}
