import {useState, useEffect, useContext, Fragment} from 'react'
import {Form, Button, Row, Col, Container} from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import UserContext from './../UserContext'

export default function Login(){
	const [email, setEmail] = useState("")
	const [pw, setPW] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)

	const { state, dispatch } = useContext(UserContext)
	const navigate = useNavigate()

	useEffect(() => {
		if(email !== "" && pw !== ""){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, pw])

	const loginUser = (e) => {
		e.preventDefault()

		fetch('https://whispering-scrubland-10370.herokuapp.com/users/login', {
			// mode: 'no-cors',
			method: "POST",
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: pw
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				localStorage.setItem('token', response.token)
				const token = localStorage.getItem("token")

				fetch('https://whispering-scrubland-10370.herokuapp.com/users/profile', {
				    // mode: 'no-cors',
					method: "GET",
				    headers:{
				        "Authorization": `Bearer ${token}`
				      }
				    })
				    .then(response => response.json())
				    .then(response => {
				      
				      localStorage.setItem('admin', response.isAdmin)

				      dispatch({type: "USER", payload: true})

				      navigate('/')
				        
				    })

				setEmail("")
				setPW("")

			} else {
				alert('Incorrect login credentials!')
			}
		})
	}


	return(
		

		<Container className="mt-5 " style={{height: "900px", width: "1020px"}}>
			
		 	<h2 className="text-center font-weight-bold mb-5">Sign in to your account</h2>
			 
			 <Row className="rowBackground no-gutters">
				 <Col lg={5}>
					 <img src='./../images/tomato.jpg' className="img-fluid loginImage"/>
				 </Col>

				 <Col lg={7} className="px-5 pt-5">
					<Form onSubmit={(e) => loginUser(e) }>

						<div >
						<Col lg={7}>
						<Form.Group className="mb-3">
							<Form.Label>Email address</Form.Label>

					    	<Form.Control 
								className="my-3 p-4"
					    		type="email" 
					    		value={email}
					    		onChange={(e) => setEmail(e.target.value)}
					    	/>
						</Form.Group>
						</Col>
						
						</div>
						
						<Col lg={7}>
						<div >
						<Form.Group className="mb-3">
					    	<Form.Label>Password</Form.Label>

					    	<Form.Control 
								className="my-3 p-4"
					    		type="password" 
					    		value={pw}
					    		onChange={(e) => setPW(e.target.value)}
					    	/>
						</Form.Group>
						</div>
						</Col>
						
						<Col lg={7}>
						<div className="">
						<Button className="button"
							variant="dark" 
							type="submit"
							disabled={isDisabled}
						>
							Login
						</Button>
						</div>
						</Col>
						
						
					</Form>
				</Col>
			 </Row>
			
			

			
		</Container>
	)
}
