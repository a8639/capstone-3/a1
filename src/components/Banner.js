

export default function Banner({bannerProp}){
	//console.log(props)
	console.log("Bannerjsprop", bannerProp)
	
	const {title, description, destination, buttonDesc} = bannerProp
	console.log("titleBanner", title)

	return(

		<div className="jumbotronBackground jumbotron jumbotron-fluid text-center">
		  <div className="container">
		    <h1 className="display-4 pb-2 font-weight-bold">{title}</h1>
		    <h4 className="lead pb-5 pt-2 font-weight-bold">{description}</h4>
		    <a href={destination} className="btn btn-dark">
		    	{buttonDesc}
		    </a>
		  </div>
		</div>

	)
}
