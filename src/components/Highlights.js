import {Fragment} from "react"
import {Card, Row, Col, Carousel, Item, Container} from "react-bootstrap"

export default function Highlights(){
	return(

		<Fragment>
		<Row className="m-5 p-5">
				<Col xs={12} md={6}>
					<Card>
					<Card.Img variant="top" src="https://images.unsplash.com/photo-1461354464878-ad92f492a5a0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" className="img-fluid" />
					  <Card.Body className="bannerHighlight">
					    <Card.Title className="text-center pb-3 bannerHighlightText">Your number one health partner</Card.Title>
					    <Card.Text className="bannerHighlightText">
					      Green Deli's mission is to make sure everyone has access to freshly harvested goods. We take pride in conducting business responsibly and supporting our farmers as well as the communities where we do busiseness. 
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={6}>
					<Card>
					<Card.Img variant="top" src="https://images.unsplash.com/photo-1471193945509-9ad0617afabf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" className="img-fluid" />
					  <Card.Body className="bannerHighlight">
					    <Card.Title className="text-center pb-3 bannerHighlightText">Be a member today!</Card.Title>
					    <Card.Text className="bannerHighlightText">
					      Register today and enjoy all the perks of being a member of our Green Deli Club! Earn points while shopping, redeem rewards, and enjoy being our lifelong partner to living a healthy life.
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
				
			</Row>
		</Fragment>
	)
}
