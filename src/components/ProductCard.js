import {useState, useEffect} from 'react'
import {Card, Row, Col, Button, CardGroup, Container} from "react-bootstrap"
import { Link } from 'react-router-dom'

export default function ProductCard({productProp}) {
	console.log("productCardProp", productProp)

	const {productName, description, price, _id} = productProp
	
	return(


		<div>

		
		
		<Card className="m-5 text-center">
			<Card.Img variant="top" src='./../images/vegetable.jpg' className="img-fluid"/>
			<Card.Body>

    		<Card.Title>{productName}</Card.Title>

    		<Card.Text>
      			{description}
    		</Card.Text>

 			<Card.Subtitle>Price(Php):</Card.Subtitle>
		    
			<Card.Text>
		    	{price}
		    </Card.Text>

   			<Button className='btn btn-success'> <Link className="buyNowButton" to={`/products/${_id}`}> Buy </Link> </Button>

  			</Card.Body>


		</Card>

		
		</div>

// <Container>
// <Row>
//     <Col lg={3}>
// 	<img variant="top" src='./../images/vegetable.jpg' className="img-fluid"/>
// 	</Col>
	
//     <Col lg={3} >
// 	<h4>{productName}</h4>
// 	<p>{description}</p>
	
// 	</Col>

//     <Col lg={3} >
// 	<p>{price}</p> 
// 	</Col>

// 	<Col lg={3} >
// 	<Button className='btn btn-success'> <Link className="buyNowButton" to={`/products/${_id}`}> Buy </Link> </Button>
	
// 	</Col>


//   </Row>
// </Container> 


		


	
	)
}
