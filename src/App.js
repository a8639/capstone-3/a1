import { useReducer, useEffect  } from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import { Container } from 'react-bootstrap'

import { UserProvider } from './UserContext'
import { initialState, reducer } from './reducer/UserReducer'

import Home from './pages/Home'
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import AppNavbar from './components/AppNavbar' 
import CreateProduct from './pages/CreateProduct';
import ProductPage from './pages/ProductPage';
import Cart from './pages/Cart';

function App() {

  const [ state, dispatch ] = useReducer(reducer, initialState)
  console.log(state)

  return(
    <UserProvider value={{ state, dispatch }} >
      <BrowserRouter>
        <AppNavbar/>          
        <Routes>
          <Route path="/" element={ <Home/> } />
          <Route path="/products" element={ <Products/> } />
          <Route path="/register" element={ <Register/> } />
          <Route path="/login" element={ <Login/> } />
          <Route path="/logout" element={ <Logout/> } />
          <Route path="/addProduct" element={ <CreateProduct /> } />
          <Route path="/products/:productId" element={< ProductPage/>} />
          <Route path="/cart" element={<Cart/>} />
        </Routes>
       
      </BrowserRouter>
    </UserProvider>

  )
}

export default App;
