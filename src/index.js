import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import { CartPage } from './components/CartPage.js'

ReactDOM.render(
  // <React.StrictMode>
    <CartPage>
      <App />
    </CartPage>
    


  // </React.StrictMode>,
  ,document.getElementById('root')
);


